# 官方网站
qemu.org

# 优点
本镜像站点速度可达10mb/s
中文教程，易懂

# 安装教程
## Windows

## 免安装包
如果系统是基于nt内核（现在用的系统一般都是）
请使用qemu/Windows_qemu/qemu.zip
如果是windows 9x（95和98）
请使用qemu/Windows_qemu/qemu_9x.zip

## 安装版本
qemu/Windows_qemu
下载（旧版本）
最新版本下载：
i386：https://qemu.weilnetz.de/w32/
amd64：https://qemu.weilnetz.de/w64/

## Linux

### Debian系安装
下载qemu/debian_amd64.deb
双击软件包或运行

```
sudo dpkg -i debian_amd64.deb

```


### 编译安装
在qemu/Source下载对应版本的源码包
x86：qemu/make_i386.sh
x64：qemu/make_amd64.sh
复制编译脚本到源代码根目录
跟着以root执行脚本即可

### 包管理器安装
到qemu/install_shell下载对应系统的安装shell文件
运行shell文件即可

## Mac OS

### 包管理器安装
到qemu/install_shell下载对应系统的安装shell文件
运行shell文件即可

### 编译安装
在qemu/Source下载对应版本的源码包
x86：qemu/make_i386.sh
x64：qemu/make_amd64.sh
复制编译脚本到源代码根目录
跟着以root执行脚本即可

# 使用教程

## 创建虚拟磁盘
`qemu-img create [-f fmt] [-o options] filename [size]`
[]内的是可选参数
其中，fmt是指文件格式，可选vdi/raw/qcow2/qcow/cow。
size是虚拟磁盘大小，不会立即分配
k代表kb，m代表mb，g代表gb，t代表tb

## 启动虚拟机

 `qemu-system-x86_64 [-boot drives] [-cd-rom filename] [-hda filename] [-fda filename] [-bios=filename] [-m ram_size]`
`drives: floppy (a), hard disk (c), CD-ROM (d), network (n)`

# qemu官方说明
https://gitee.com/giggle-haha/qemu/blob/master/help.txt